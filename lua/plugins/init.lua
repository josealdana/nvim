return {
  {
    "dracula/vim",
    config = function()
      vim.cmd.colorscheme("dracula")
    end,
  },
  { "ptzz/lf.vim" },
  { "voldikss/vim-floaterm" },
  { "fatih/vim-go" },
  { "andymass/vim-matchup" },
  { "jwalton512/vim-blade" },
  { "psf/black" },
  {
    "numToStr/Navigator.nvim",
    lazy = true,
    config = function()
      require("Navigator").setup({
        auto_save = "current",
      })
    end,
    keys = {
      { "<C-h>", "<CMD>NavigatorLeft<CR>" },
      { "<C-l>", "<CMD>NavigatorRight<CR>" },
      { "<C-k>", "<CMD>NavigatorUp<CR>" },
      { "<C-j>", "<CMD>NavigatorDown<CR>" },
    },
  },
}
